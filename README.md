# wispeer

Wispeer is an decentralized and distributed social network.

There are multiple git repo :

**wispeer-client**: this repo. This is the implementation os client-side wipeer protocol. It's the library you may want to use if you want to create your own client (as a website or a bot for instance). This can also be used for other network aplications.

**wispeer-tracker**: https://framagit.org/wispeer/wisper-tracker This is the tracker code implementing the server-side wispeer protocol. If you want to host a tracker (aka tag) you may use this.

**wispeer-gui**: https://framagit.org/wispeer/wispeer-gui Contains the code of the wispeer gui using angular 4 and bootstrap 4.

**wispeer-www-build**: https://framagit.org/wispeer/wispeer-www-build Contains build of wispeer-gui. If you want to host a copy of the website this is your repo.

## wispeer network

A Wispeer client finds other wispeer clients via trackers. A client is connected to a random batch of peers and a random batch of peers are connected to it. (those random batches are differents). When a client sends a message, it is sent to all peers the client is connected to. The client receive message from peers connected to it. No messages are sent to any tracker or server.

Connection to the tracker is made via (secure) websocket and connection between peers are via webRTC. Media are sent via webtorrent.

## wispeer-client

Wispeer-client is a javascript library made for the browser (but should works in node with few modifications)
All js code follow coding style rules 'standard'.

### api


api is exported as a node modul but also present on the global object window.wispeer

```javascript
getConf()
```

Return a list of all trackers with the format : {url: 'wss://torrero.fr/wispeer-tracker', domain: 'wis-peer', name: 'global en', maxPeers: 50}

```javascript
sendMessage(trackerId, message)
```

send the object message to the tracker with id trackerId. 'message' can be any object. It can have a File object in message.toTorrent that will be sent via webtorrent instead of direct webrtc. Stringify-ed message have a max length of 5Mo (including torrent which can be huge in the case of a video)

```javascript
setMessageCB(cb)
```

Set a function callback to receive messages. cb is a function cb(message, trackerId). messages are the same as sent in sentMessage except for the 'toTorrent' field which is changed by a field 'stream' which contains a webtorrent file that can be streamed. (see webtorrent documentation : https://github.com/webtorrent/webtorrent/blob/master/docs/api.md#file-api)

```javascript
setOnMetaChangeCB(cb)
```

Set a function callback to receive trackers status informations. cb is a function cb(metaData, trackerId)

```javascript
addTracker(url, domain, name, maxPeers)
```

call this to add a tracker. Domain shoud be 'wis-peer' for now.

```javascript
setDissmissedOnMessage(message, dissmissed)
```

Set if a message should be hide or not. 'dissmissed' is a boolean


