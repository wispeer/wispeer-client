var network = require('./lib/network.js')
var config = require('./lib/config.js')
var webtorrentService = require('./lib/webtorrentService.js')

var api = {}
var privateApi = {}

var net = []

api.getConf = config.getConf

api.sendMessage = function (trackerId, message) {
  if (trackerId >= net.length || trackerId < 0) {
    console.log('send message: wrong tracker id ' + trackerId + ' should be in [0, ' + (net.length - 1) + ']')
    return
  }
  net[trackerId].sendMessage(message)
}

api.setMessageCB = function (cb) {
  var setCB = function (i) {
    net[i].setMessageCB(function (message) {
      cb(message, i)
    })
  }

  for (var i = 0; i < net.length; i++) {
    setCB(i)
  }
}

api.setOnMetaChangeCB = function (cb) {
  var setCB = function (i) {
    net[i].setOnMetaChangeCB(function (metaData) {
      cb(metaData, i)
    })
  }

  for (var i = 0; i < net.length; i++) {
    setCB(i)
  }
}

api.addTracker = function (url, domain, name, maxPeers) {
  config.addTracker(url, domain, name, maxPeers)
  net[net.length] = network.createNetwork(url, domain, webtorrentService)
  return net.length - 1
}

api.setDissmissedOnMessage = function (message, dissmissed) {
  if (window.localStorage.savedMessages) {
    var savedMessages = JSON.parse(window.localStorage.savedMessages)
    var i = 0
    while (i < savedMessages.length) {
      var tmpMessage = JSON.parse(savedMessages[i])
      if (tmpMessage.message === message.message && tmpMessage.author === message.author) {
        tmpMessage.dissmissed = dissmissed
        savedMessages[i] = JSON.stringify(tmpMessage)
      }
      i++
    }
    window.localStorage.savedMessages = JSON.stringify(savedMessages)
  }
}

privateApi.init = function () {
  var conf = config.getConf()
  for (var i = 0; i < conf.trackers.length; i++) {
    net[net.length] = network.createNetwork(conf.trackers[i].url, conf.trackers[i].domain, webtorrentService)
  }
}

privateApi.init()

api.net = api

module.exports = api

window.wispeer = api
