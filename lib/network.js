var W3CWebSocket = require('websocket').w3cwebsocket
var Peer = require('simple-peer')

var ctx = {}

ctx.createNetwork = function (tracker, trackerDomain, webtorrentService) {
  var client = null

  var maxPeers = 50
  var maxDataSize = 5 * 1024 * 1024

  function initTracker (tracker, trackerDomain) {
    if (!tracker) {
      tracker = 'wss://torrero.fr/wispeer-tracker'
    }
    if (!trackerDomain) {
      trackerDomain = 'wis-peer'
    }
    log('connection to the tracker ' + tracker)
    client = new W3CWebSocket(tracker, trackerDomain)
    metaData.trackerStatus = 'unknown'
    callMetaDataCBs()

    client.onerror = function () {
      log('Connection Error')
      metaData.trackerStatus = 'error'
      callMetaDataCBs()
    }

    client.onopen = function () {
      log('connected to tracker')
      currentWaitingPeer = initMyPeer(client)
      setTimeout(function () {
        askForMorePeers(client)
      }, 10000)
      metaData.trackerStatus = 'opened'
      callMetaDataCBs()
    }

    client.onclose = function () {
      log('tracker connection closed')
      metaData.trackerStatus = 'closed'
      callMetaDataCBs()
      setTimeout(function () {
        initTracker(tracker, trackerDomain)
      }, 5000 + Math.random(0, 10000))
    }

    client.onmessage = function (e) {
      if (typeof e.data === 'string') {
        log("Received: '" + e.data + "'")
        parseMessage(e.data, client)
      } else {
        log('Receive message of type ' + typeof e.data)
      }
    }
  }

  function createNewPeer (client, id, offer, onClose) {
    var p = new Peer(
      {
        initiator: false,
        trickle: false
      })

    p.on('error', function (err) {
      log('error', err)
    })

    p.on('signal', function (data) {
      log('SIGNAL', JSON.stringify(data))
      sendOfferTo(client, id, JSON.stringify(data))
    })

    p.on('connect', function () {
      log('CONNECT')
    })

    if (!doIKnowHim(id)) {
      p.on('data', function (data) {
        log('data: ' + data)
        log('datalen: ' + data.length)
        if (data.length <= maxDataSize) {
          onData(data)
        } else {
          log('received message too large')
        }
      })
    }

    p.on('close', function () {
      onClose()
    })

    p.id = id

    p.signal(JSON.parse(offer))

    return p
  }

  function initMyPeer (client) {
    var p = new Peer(
      {
        initiator: true,
        trickle: false
      })

    p.on('error', function (err) {
      log('error', err)
    })

    p.on('signal', function (data) {
      log('SIGNAL', JSON.stringify(data))
      sendOffer(client, JSON.stringify(data))
    })

    p.on('connect', function () {
      log('CONNECT')
    })

    p.on('data', function (data) {
      log('data: ' + data)
    })
    p.on('close', function () {
      if (currentWaitingPeer === p) {
        currentWaitingPeer = initMyPeer(client)
      }
    })

    return p
  }

  var peers = {}
  var nbPeers = 0
  var incPeers = []
  var onDataCBs = []
  var savedMessages = []
  var onMetaChangeCBs = []
  var metaData = {}
  metaData.maxPeers = maxPeers

  function checkTrackersInMessages () {
    for (var i = 0; i < savedMessages.length; i++) {
      var found = false
      var trackers = JSON.parse(savedMessages[i]).trackers
      if (trackers) {
        for (var j = 0; j < trackers.length; j++) {
          console.log('check tracker ' + trackers[j].url + ' with ' + tracker)
          if (trackers[j].url === tracker) {
            found = true
          }
        }
      }
      if (!found) {
        savedMessages.splice(i, 1)
        console.log('message removed')
      }
    }
  }

  if (window.localStorage.savedMessages && window.localStorage.savedMessages != null) {
    savedMessages = JSON.parse(window.localStorage.savedMessages)
    checkTrackersInMessages()
    log('loaded : ' + window.localStorage.savedMessages)
  }

  var currentWaitingPeer

  function doIKnowHim (id) {
    return peers[id]
  }

  function onData (data) {
    var next = function () {
      for (var i = 0; i < onDataCBs.length; i++) {
        onDataCBs[i].cb(data)
      }
    }
    var dataParsed = JSON.parse(data)
    if (dataParsed.torrent) {
      webtorrentService.getTorrent(new Buffer(dataParsed.torrent, 'base64'), function (stream) {
        dataParsed.stream = stream
        data = dataParsed
        next()
      })
    } else {
      data = dataParsed
      next()
    }
  }

  function toBase64 (data) {
    return new Buffer(data).toString('base64')
  }

  function toAscii (data) {
    return new Buffer(data, 'base64').toString('ascii')
  }

  function sendOffer (client, offer) {
    client.send('0' + '-' + toBase64(offer))
  }

  function sendOfferTo (client, id, offer) {
    client.send('1' + '-' + id + '-' + toBase64(offer))
  }

  function askForMorePeers (client) {
    if (metaData.trackerStatus === 'closed') {
      return
    }
    log('nb peers: ' + nbPeers)
    if (nbPeers < maxPeers) {
      log('ask for more peers !')
      client.send('2')
    }
    setTimeout(function () {
      askForMorePeers(client)
    }, 5000 + Math.random(0, 10000))
  }

  function parseMessage (message, client) {
    var messageSplited = message.split('-')
    var offer = null
    if (messageSplited[1]) {
      var id = messageSplited[0]
      offer = toAscii(messageSplited[1])

      if (doIKnowHim(id)) {
        createNewPeer(client, id, offer, function () {})
      } else {
        peers[id] = createNewPeer(client, id, offer, function () {
          peers[id] = null
          nbPeers--
          metaData.nbPeers = nbPeers
          callMetaDataCBs()
        })
        nbPeers++
        metaData.nbPeers = nbPeers
        callMetaDataCBs()
      }
    } else {
      offer = toAscii(messageSplited[0])
      currentWaitingPeer.signal(JSON.parse(offer))
      var peerToAdd = currentWaitingPeer
      incPeers[incPeers.length] = currentWaitingPeer
      log('catching up')
      for (var i = 0; i < savedMessages.length; i++) {
        var messageToSend = savedMessages[i]
        logMessage('sending message ' + savedMessages[i])
        setTimeout(function () {
          peerToAdd.send(messageToSend)
        }, 1000)
      }
      currentWaitingPeer = initMyPeer(client)
    }
  }

  function callMetaDataCBs () {
    for (var i = 0; i < onMetaChangeCBs.length; i++) {
      onMetaChangeCBs[i](metaData)
    }
  }

  var api = {}

  api.initTracker = initTracker

  api.sendMessage = function (message) {
    var next = function () {
      message = JSON.stringify(message)
      onData(message)
      savedMessages[savedMessages.length] = message
      window.localStorage.savedMessages = JSON.stringify(savedMessages)
      for (var i = 0; i < incPeers.length; i++) {
        incPeers[i].send(message)
      }
    }

    if (message.toTorrent) {
      webtorrentService.createTorrent(message.toTorrent, function (torrent) {
        message.toTorrent = undefined
        message.torrent = torrent.torrentFile.toString('base64')
        next()
      })
    } else {
      next()
    }
  }

  api.setMessageCB = function (cb) {
    var callback = {}
    callback.cb = cb

    onDataCBs[onDataCBs.length] = callback
    for (var i = 0; i < savedMessages.length; i++) {
      var messageToSend = savedMessages[i]
      logMessage('recall message ' + messageToSend)
      cb(JSON.parse(messageToSend))
    }
  }

  api.unscream = function (message) {
    var messageParsed = JSON.parse(message)
    for (var i = 0; i < savedMessages.length; i++) {
      if (messageParsed.message === JSON.parse(savedMessages[i]).message &&
                messageParsed.author === JSON.parse(savedMessages[i]).author) {
        savedMessages.splice(i, 1)
      }
    }
    window.localStorage.savedMessages = JSON.stringify(savedMessages)
  }

  api.setOnMetaChangeCB = function (cb) {
    onMetaChangeCBs[onMetaChangeCBs.length] = cb
    cb(metaData)
  }

  initTracker(tracker, trackerDomain)

  return api
}

function log (message) {
    // console.log(message)
}

function logMessage (message) {
    // console.log(message)
}

module.exports = ctx
