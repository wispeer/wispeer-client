var api = {}
var privateApi = {}

var ctx = {}
ctx.conf = null


api.getConf = function()
{
	if (!ctx.conf)
	{
		privateApi.load()
	}
	return ctx.conf
}

api.addTracker = function(url, domain, name, maxPeers)
{
	if (!ctx.conf)
	{
		privateApi.load()
	}
	ctx.conf.trackers[ctx.conf.trackers.length] = {url: url, domain: domain, name: name, maxPeers: maxPeers}
	privateApi.save()
}

api.removeTracker = function(id)
{
	if (!ctx.conf)
	{
		privateApi.load()
	}
	if (ctx.conf.trackers.length > id)
	{
		ctx.conf.trackers.splice(id, 1)
		privateApi.save()
	}
}


privateApi.load = function()
{
	if (window.localStorage.wispeerConf)
	{
		ctx.conf = JSON.parse(window.localStorage.wispeerConf)
		if (!ctx.conf || !ctx.conf.trackers)
		{
			privateApi.createDefaultConf()
			privateApi.save()
		}
	}
	else
	{
		privateApi.createDefaultConf()
		privateApi.save()
	}
}

privateApi.save = function()
{
	window.localStorage.wispeerConf = JSON.stringify(ctx.conf)
}

privateApi.createDefaultConf = function()
{
	ctx.conf = {}
	ctx.conf.trackers = []
	ctx.conf.trackers[0] = {url: 'wss://torrero.fr/wispeer-tracker', domain: 'wis-peer', name: 'global en', maxPeers: 50}
}

module.exports = api
