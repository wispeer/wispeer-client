var WebTorrent = require('webtorrent')
var parseTorrent = require('parse-torrent')

var webtorrent = new WebTorrent()

var api = {}
var ctx = {}

ctx.torrent = {}

api.createTorrent = function (file, cb) {
  var t = webtorrent.seed(file, function (torrent) {
    if (!ctx.torrent[file]) {
      ctx.torrent[file] = {}
    }
    if (!ctx.torrent[file].torrent) {
      ctx.torrent[file].torrent = torrent
    }
    if (ctx.torrent[file].cbSeed) {
      for (var i = 0; i < ctx.torrent[file].cbSeed.length; i++) {
        ctx.torrent[file].cbSeed[i](torrent)
      }
    }
    cb(torrent)
  })
  t.on('error', function (err) {
    console.log(err)
    if (ctx.torrent[file] && ctx.torrent[file].torrent) {
      cb(ctx.torrent[file].torrent)
    } else {
      if (!ctx.torrent[file]) {
        ctx.torrent[file] = {}
      }
      if (!ctx.torrent[file].cbSeed) {
        ctx.torrent[file].cbSeed = []
      }
      ctx.torrent[file].cbSeed[ctx.torrent[file].cbSeed.length] = cb
    }
  })
}

api.getTorrent = function (torrent, cb) {
  var hash = parseTorrent(torrent).infoHash

  if (ctx.torrent[hash]) {
    if (ctx.torrent[hash].stream) {
      cb(ctx.torrent[hash].stream)
    } else {
      if (ctx.torrent[hash].cb) {
        ctx.torrent[hash].cb[ctx.torrent[hash].cb.length] = cb
      } else {
        ctx.torrent[hash].cb = [cb]
      }
    }
    return
  }
  ctx.torrent[hash] = {
    torrent: torrent
  }

  var t = webtorrent.add(torrent, function (torrent2) {
    ctx.torrent[hash].stream = torrent2.files[0]
    if (ctx.torrent[hash].cb) {
      for (var i = 0; i < ctx.torrent[hash].cb.length; i++) {
        ctx.torrent[hash].cb[i](ctx.torrent[hash].stream)
      }
    }
    ctx.torrent[hash].cb = []
    cb(ctx.torrent[hash].stream)
  })
  t.on('error', function (err) {
    console.log(err)
    if (ctx.torrent[hash]) {
      return ctx.torrent[hash].stream
    }
    cb(null)
  })
}

module.exports = api
